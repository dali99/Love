<!DOCTYPE HTML>
<html>
<head>
		<title>Christmas Calendar</title>
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

	<!--jQuery-->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/smoothness/jquery-ui.css" />
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
	<!-- Bootstrap -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<!-- Theme -->
		<!-- Material Design https://github.com/FezVrasta/bootstrap-material-design --
		<link rel="stylesheet" type="text/css" href="Resources/Libraries/bootstrap-material-design/css-compiled/material.css">
		<link rel="stylesheet" type="text/css" href="Resources/Libraries/bootstrap-material-design/css-compiled/material-wfont.css">
		<link rel="stylesheet" type="text/css" href="Resources/Libraries/bootstrap-material-design/css-compiled/ripples.css">
		<script src="Resources/Libraries/bootstrap-material-design/scripts/material.js"></script>
		<script src="Resources/Libraries/bootstrap-material-design/scripts/ripples.js"></script>
		<!-- Font -->
		<link rel="stylesheet" type="text/css" href="Resources/Libraries/bootstrap-material-design/icons/icons-material-design.css">

		<link rel="stylesheet" type="text/css" href="Resources/css/Squares.css">

	<!-- Custom Stylesheets -->
		<link rel="stylesheet" type="text/css" href="Resources/css/main.css">
		<link rel="stylesheet" type="text/css" href="Resources/css/Calendar.css">

		<script type="text/javascript" src="Resources/js/Calendardate.js"></script>
</head>
<body>

<a href="Calendar/2014/01.php"><div class="square bg" id="1">
    <div class="content">
        <div class="table">
            <div class="table-cell">
                1
            </div>
        </div>
    </div>
</div></a>
<a href="Calendar/2014/two.php"><div class="square bg" id="2">
    <div class="content">
        <div class="table">
            <div class="table-cell">
                2
            </div>
        </div>
    </div>
</div></a>
<a href="Calendar/2014/III.php"><div class="square bg" id="3">
    <div class="content">
        <div class="table">
            <div class="table-cell">
                3
            </div>
        </div>
    </div>
</div></a>
<a href="Calendar/2014/100.php"></a><div class="square bg" id="4">
    <div class="content">
        <div class="table">
            <div class="table-cell">
                4
            </div>
        </div>
    </div>
</div></a>
<a href="Calendar/2014/11111.php"><div class="square bg" id="5">
    <div class="content">
        <div class="table">
            <div class="table-cell">
                5
            </div>
        </div>
    </div>
</div></a>
<a href="Calendar/2014/........php"><div class="square bg" id="6">
    <div class="content">
        <div class="table">
            <div class="table-cell">
                6
            </div>
        </div>
    </div>
</div></a>
<a href="Calendar/2014/7.php"><div class="square bg" id="7">
    <div class="content">
        <div class="table">
            <div class="table-cell">
                7
            </div>
        </div>
    </div>
</div></a>
<a href="Calendar/2014/08.php"><div class="square bg" id="8">
    <div class="content">
        <div class="table">
            <div class="table-cell">
                8
            </div>
        </div>
    </div>
</div></a>
<a href="Calendar/2014/09.php" class="passed"><div class="square bg" id="9">
    <div class="content">
        <div class="table">
            <div class="table-cell">
                <p>9</p>
                <a href="anniversary/4months.php" class="Green"><button type="button" class="btn btn-default Green" style="z-index: 200;">I love you!</button></a>
                <a href="Calendar/2014/10.php" class="Green"><button type="button" class="btn btn-default Green" style="z-index: 200;">Actual Calendar button</button></a>
            </div>
        </div>
    </div>
</div></a>
<a href="Calendar/2014/10.php"><div class="square bg" id="10">
    <div class="content">
        <div class="table">
            <div class="table-cell">
                10
            </div>
        </div>
    </div>
</div></a>
<a href="Calendar/2014/11.php"><div class="square bg" id="11">
    <div class="content">
        <div class="table">
            <div class="table-cell">
                11
            </div>
        </div>
    </div>
</div></a>
<a href="Calendar/2014/12.php"><div class="square bg" id="12">
    <div class="content">
        <div class="table">
            <div class="table-cell">
                12
            </div>
        </div>
    </div>
</div></a>
<a href="Calendar/2014/13.php"><div class="square bg" id="13">
    <div class="content">
        <div class="table">
            <div class="table-cell">
                13
            </div>
        </div>
    </div>
</div></a>
<a href="Calendar/2014/14.php"><div class="square bg" id="14">
    <div class="content">
        <div class="table">
            <div class="table-cell">
                14
            </div>
        </div>
    </div>
</div></a>
<a href="Calendar/2014/15.php"><div class="square bg" id="15">
    <div class="content">
        <div class="table">
            <div class="table-cell">
                15
            </div>
        </div>
    </div>
</div></a>
<a href="Calendar/2014/16.php"><div class="square bg" id="16">
    <div class="content">
        <div class="table">
            <div class="table-cell">
                16
            </div>
        </div>
    </div>
</div></a>
<a href="Calendar/2014/17.php"><div class="square bg" id="17">
    <div class="content">
        <div class="table">
            <div class="table-cell">
                17
            </div>
        </div>
    </div>
</div></a>
<a href="Calendar/2014/18.php"><div class="square bg"  id="18">
    <div class="content">
        <div class="table">
            <div class="table-cell">
                18
            </div>
        </div>
    </div>
</div></a>
<a href="Calendar/2014/19.php"><div class="square bg" id="19">
    <div class="content">
        <div class="table">
            <div class="table-cell">
                19
            </div>
        </div>
    </div>
</div></a>
<a href="Calendar/2014/20.php"><div class="square bg" id="20">
    <div class="content">
        <div class="table">
            <div class="table-cell">
                20
            </div>
        </div>
    </div>
</div></a>
<a href="Calendar/2014/21.php"><div class="square bg" id="21">
    <div class="content">
        <div class="table">
            <div class="table-cell">
                21
            </div>
        </div>
    </div>
</div></a>
<a href="Calendar/2014/22.php"><div class="square bg" id="22">
    <div class="content">
        <div class="table">
            <div class="table-cell">
                22
            </div>
        </div>
    </div>
</div></a>
<a href="Calendar/2014/23.php"><div class="square bg" id="23">
    <div class="content">
        <div class="table">
            <div class="table-cell">
                23
            </div>
        </div>
    </div>
</div></a>
<a href="Calendar/2014/24.php"><div class="square bg" id="24">
    <div class="content">
        <div class="table">
            <div class="table-cell">
                24!
            </div>
        </div>
    </div>
</div></a>


</body>
</html>