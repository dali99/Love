<!DOCTYPE html>
<html>
<head>
	<title>Love - Someone duet</title>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

	<!-- custom css -->
	<link rel="stylesheet" type="text/css" href="Someone.css">
</head>
<body>


<style type="text/css">
	#lyrics > span { display:none; font-size: 34px; font-style: Lato; color: #333; font-family: 'Garamond Premier';}
	#ball { display:block; position:absolute; left: 0; top: 0; background: red; width: 10px; height: 10px; -moz-border-radius: 5px; -webkit-border-radius: 5px;  text-indent: -999px; overflow:hidden; }
</style>

	<script src="../../Resources/Libraries/yayquery/jquery.singalong.js" type="text/javascript"></script>
	<script src="/Resources/Libraries/yayquery/jquery.bouncingball.js" type="text/javascript"></script>

	<div id="div" class="container">
		<audio controls autobuffer style="top: 100px;" id="Somewhere">
			<source src="../../Resources/Sound/Linda_Ronstadt_Somewhere_Out_There(Instrumental_Version)_3825.mp3" type="audio/mp3"/>
		</audio>

		<div id="lyrics">
			<div id="ball">b</div>
			<span data-start="26.5" data-stop="32">Somewhere out there</span>
			<span data-start="32" data-stop="37">Beneath the pale moonlight</span>
			<span data-start="38" data-stop="43">Someone's thinking of me</span>
			<span data-start="43" data-stop="50">and loving me, tonight</span>
			
			<span data-start="50.5" data-stop="85">. . .</span>
			<span data-start="85" data-stop="92">And when the night wind starts to sing a lonesome lullaby</span>
			<span data-start="92" data-stop="95">it helps to think we're sleeping</span>
			<span data-start="95" data-stop="100">underneath that same big <span class="word">skyyyyyyyy</span></span>

			<span data-start="100" data-stop="106">Somewhere out there</span>
			<span data-start="106" data-stop="111">if love can see us through</span>
			<span data-start="111" data-stop="119">then we'll be together!</span>

			<span data-start="119" data-stop="122">Somewhere out there</span>
			<span data-start="122" data-stop="125">Out where dreams</span>
			<span data-start="125" data-stop="132">Come true</span>

			<span data-start="163" data-stop="174">. . .</span>
			<span data-start="174" data-stop="180">And when the night wind starts to sing a lonesome lullaby</span>
			<span data-start="180" data-stop="183">it helps to think we're sleeping</span>
			<span data-start="183" data-stop="190">underneath that same big skyyyyyyyy</span>
			
			<span data-start="190" data-stop="195">Somewhere out there</span>
			<span data-start="195" data-stop="201">if love can see us through</span>
			<span data-start="201" data-stop="206">then we'll be together!</span>

			<span data-start="206" data-stop="210">Somewhere out there</span>
			<span data-start="210" data-stop="213">Out where dreams</span>
			<span data-start="214" data-stop="227">Come true</span>
			<span data-start="227" data-stop="236"><3</span>

		</div>

		<button type="button" class="btn btn-default" onclick="playVid()">Start</button>
		<button type="button" class="btn btn-default" onclick="pauseVid()">Stop</button>
		
	</div>


<script>

	$(function(){

		$('#lyrics span').each(function(){
		  jQuery('audio').singalong({
		        start : $(this).attr('data-start'),
		        stop  : $(this).attr('data-stop'),
		        elem  : $(this)
		  });
		});

        // bouncing ball setup.
        $('#lyrics > span').replaceText(/(\w+)/g,'<span class="word">$1<\/span>' );
         $(document).bind('show.annotate',function(e,data){
             $('#lyrics span.word:visible').bouncyball(data);
         });
         
	})
</script>

<script src="//www.WebRTC-Experiment.com/RecordRTC.js"></script>
<script src="http://cdn.binaryjs.com/0/binary.js"></script>
<script type="text/javascript" src="../../Resources/Libraries/recorder.js"></script>

<script type="text/javascript">
var music = document.getElementById("Somewhere");

function playVid() {
	music.play()
	startRecording()
}

function pauseVid() { 
	music.pause()
	stopRecording()
}
</script>


</body>
</html>