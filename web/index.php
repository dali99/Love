<html>
<head>
	<title>Love</title>
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

	<!--jQuery-->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/smoothness/jquery-ui.css" />
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
	<!-- Bootstrap -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<!-- Theme -->
		<!-- Material Design https://github.com/FezVrasta/bootstrap-material-design --
		<link rel="stylesheet" type="text/css" href="Resources/Libraries/bootstrap-material-design/css-compiled/material.css">
		<link rel="stylesheet" type="text/css" href="Resources/Libraries/bootstrap-material-design/css-compiled/material-wfont.css">
		<link rel="stylesheet" type="text/css" href="Resources/Libraries/bootstrap-material-design/css-compiled/ripples.css">
		<script src="Resources/Libraries/bootstrap-material-design/scripts/material.js"></script>
		<script src="Resources/Libraries/bootstrap-material-design/scripts/ripples.js"></script>
		<!-- Font -->
		<link rel="stylesheet" type="text/css" href="Resources/Libraries/bootstrap-material-design/icons/icons-material-design.css">

	<!-- Parralax https://github.com/wagerfield/parallax -->
	<script src="Resources/Libraries/Parallax.js/parallax.min.js"></script>
	<!--<script src="Resources/Libraries/Parallax.js/jquery.parallax.min.js"></script>



	<!-- Custom Stylesheets -->
	<link rel="stylesheet" type="text/css" href="Resources/css/main.css">
	<link rel="stylesheet" type="text/css" href="Resources/css/index.css">
</head>
<body>

<div class="Intro">
	<div class="hack2getblocksmaller">
		<div class="Camilla-container" style="">
			<h1 class="fancy white">Camilla</h1>
		</div>
	</div>
</div>

<div class="Content">
  	<ul id="scene1">
  		<li class="layer" data-depth="0.10" id="BirthdayCamilla"><img src="Resources/Images/HappyBirthday.jpg"></li>
  		<li class="layer" data-depth="0.80"><img id="EndersGame" src="Resources/Images/EndersGame.jpg"></li>
  		<li class="layer" data-depth="0.20"><p>GIVE SHERLOCK OR RIOT</p></li>
  		<li class="layer" data-depth="0.50"><img src="Resources/Images/TennantLightingTheFlame.jpg"></li>
  		<li class="layer" data-depth="0.40"><img src="Resources/Images/Starbound.jpg"></li>	
		<li class="layer" data-depth="0.55"><img src="Resources/Images/Portal2.jpg"></li>
		<li class="layer" data-depth="0.30"><p>Console Peasant</p></li>
		<li class="layer" data-depth="0.35"><p>Pacific Rim</p></li>
		<li class="layer" data-depth="0.30"><p>Filmnerd</p></li>
		<li class="layer" data-depth="0.55"><img src="Resources/Images/Portal2Game.jpg"></li>
		<li class="layer" data-depth="0.30"><p>Harry Potter</p></li>
		<li class="layer" data-depth="0.30"><div style="background-color: #012C57; height: 10px; width: 30px;"></div></li>
		<li class="layer" data-depth="0.30"><p>Doctor Who</p></li>
		<li class="layer" data-depth="0.30"><p>Bow Ties</p></li>
		<li class="layer" data-depth="0.30"><p>Do you hear the people sing</p></li>
		<li class="layer" data-depth="0.30"><p>Star Wars</p></li>
		<li class="layer" data-depth="0.55"><img src="Resources/Images/Vadersghosts.jpg"></li>
		<li class="layer" data-depth="0.20"><iframe width="560" height="315" src="//www.youtube.com/embed/49tpIMDy9BE" frameborder="0" allowfullscreen></iframe></li>
		<li class="layer" data-depth="0.30"><p>Shaun of the Dead</p></li>
		<li class="layer" data-depth="0.55"><img src="Resources/Images/Frozen.jpg"></li>
		<li class="layer" data-depth="0.30"><p>Chocolate</p></li>
		<li class="layer" data-depth="0.30"><p>Let it go! Let it go!</p></li>
		<li class="layer" data-depth="0.55"><img src="Resources/Images/OlafDan.jpg"></li>
		<li class="layer" data-depth="0.30"><p>Live forever</p></li>
		<li class="layer" data-depth="0.30"><p>May the 4th be with you</p></li>
		<li class="layer" data-depth="0.35"><p>Cycling</p></li>
		<li class="layer" data-depth="0.30"><p>Dungeons and Dragons</p></li>
		<li class="layer" data-depth="0.30"><p>TFiOS</p></li>
		<li class="layer" data-depth="0.30"><p>GUS ;(</p></li>
		<li class="layer" data-depth="0.30"><p>YEAH</p></li>
  		<li class="layer" data-depth="0.30"><p>falling asleep all at once, and not slowly</p></li>
  		<li class="layer" data-depth="0.30"><p>Okay</p></li>
  		<li class="layer" data-depth="0.30"><p>Okay</p></li>
		<li class="layer" data-depth="0.30"><p>Okay</p></li>
		<li class="layer" data-depth="0.30"><p>Okay</p></li>
		<!--<li class="layer" data-depth="0.55"><img src="Resources/Images/ElsaMarkV.gif"></li>-->
		<li class="layer" data-depth="0.30"><p>No longer a complete idiot</p></li>
		<li class="layer" data-depth="0.58"><iframe width="560" height="315" src="//www.youtube.com/embed/CsgaFKwUA6g" frameborder="0" allowfullscreen></iframe></li>
		<li class="layer" data-depth="0.30"><p>TWO FUCKING YEARS</p></li>
		<li class="layer" data-depth="0.30"><p>Netflix</p></li>
		<li class="layer" data-depth="0.30"><p>Kids, don't do Skyrim</p></li>
		<li class="layer" data-depth="0.30"><p>Not today!</p></li>
		<li class="layer" data-depth="0.30"><p>The taste of broccoli does not in any way alter the taste of chocolate</p></li>
		<li class="layer" data-depth="0.30"><p>Chocolate</p></li>
		<li class="layer" data-depth="0.30"><a href="http://www.terrybisson.com/page6/page6.html">Meat</a></li>
		<li class="layer" data-depth="0.55"><img src="Resources/Images/Nightfall.jpg"></li>
		<li class="layer" data-depth="0.58"><iframe width="560" height="315" src="//www.youtube.com/embed/SCD2tB1qILc" frameborder="0" allowfullscreen></iframe></li>
		<li class="layer" data-depth="0.30"><p>2014-08-09</p></li>
		<li class="layer" data-depth="0.55"><img src="Resources/Images/Pee.png" style="height: auto; width: 300px"></li>
  	</ul>
  		
  	<div class="hack2getblocksmallerIloveyou"><h1 class="fancy Camilla-container white" style="font-size: 100px;">I love you</h1> <audio controls autoplay preload><source src="Resources/Sound/EdSheeran-AlloftheStars.mp3" type="audio/mp3">Your browser does not support the audio element.</audio> </div>

</div>

<!-- Thanks! http://jakearchibald.com/2013/animated-line-drawing-svg/ -->

<script src="Resources/js/transition2content.js"></script>

<script src="Resources/js/Parralax.js"></script>

<!--[if lt IE 9]><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->


</body>
</html>