(function() {
  function toArray(arr) {
    return Array.prototype.slice.call(arr);
  }

  var svg = document.querySelector('.Camilla-container svg');
  svg.parentNode.style.display = 'block';
  svg.style.visibility = 'hidden';

  function fullSketch() {
    svg.style.visibility = '';
    var paths = toArray(svg.querySelectorAll('path'));
    var begin = 0;

    var durations = paths.map(function(path) {
      var length = path.getTotalLength();
      var className = path.getAttribute('class') || '';

      path.style.strokeDasharray = length + ' ' + length;
      path.style.strokeDashoffset = length;

      // no classList on svg elements in Safari :(
      if (className.indexOf('letter') != -1) {
        return Math.pow(length, 0.5) * 0.02;
      }
      else {
        return Math.pow(length, 0.5) * 0.03;
      }
    });

    paths[0].getBoundingClientRect();

    paths.forEach(function(path, i) {
      path.style.transition = path.style.WebkitTransition = 'stroke-dashoffset ' + durations[i] + 's ' + begin + 's ease-in-out';
      path.style.strokeDashoffset = '0';
      begin += durations[i] + 0.1;
    });
  }

  window.addEventListener('load', fullSketch);
}());