/**
 * Created by noamc on 8/31/14.
 modified by dan
 */
var binaryServer = require('binaryjs').BinaryServer;
var wav = require('wav');
var exec = require('child_process').exec;

var connect = require('connect');
var serveStatic = require('serve-static');
connect().use(serveStatic('.')).listen(8080);

var server = binaryServer({port: 9001});


var path = 'recordings/';


server.on('connection', function(client) {
    console.log("new connection...");
    var fileWriter = null;

    client.on('stream', function(stream, meta) {
        console.log("Stream Start")
		fileName = new Date().getTime() + ".wav";
        fileNamePath = path + fileName;
        var fileWriter = new wav.FileWriter(fileNamePath, {
            channels: 1,
            sampleRate: 44100,
            bitDepth: 16
        });

        stream.pipe(fileWriter);
    });


    client.on('close', function() {
        if (fileWriter != null) {
            fileWriter.end();
			console.log("FileWriter end");
        }
		var convertCommand = 'ffmpeg -i ' + fileNamePath + ' -b:a 192k ' + fileNamePath + '.converted.mp3';
		exec(convertCommand);
        console.log("convertCommand");
		var mergeCommand = 'ffmpeg -i "concat:' + fileNamePath + '.converted.mp3|tomerge/dan.mp3" -acodec copy ../web/anniversary/6/Download/' + fileName + '.merged.mp3';
		exec(mergeCommand);
        console.log("mergeCommand");
		console.log("Connection Closed");
		console.log(fileName);
    });
})